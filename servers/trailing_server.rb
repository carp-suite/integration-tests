require 'sinatra'
require 'json'

get '/api/prefix/*' do |path|
  headers = {
    'Set-Cookie' => 'cookie1=name;Path=/',
    'Content-Type' => 'application/json'
  }
  [200, headers, JSON.pretty_generate({ path: "/#{path}" })]
end
