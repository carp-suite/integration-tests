# typed: true

require 'sinatra'

get '/' do
  sleep(ENV['DELAY'].to_i)

  'HELLO WORLD'
end
