# typed: true

require 'sinatra'

def prepare_headers(headers)
  headers.clear
  headers['CARP_URL'] = request.path
  headers['CARP_QUERY_STRING'] = request.query_string

  env.each do |header, value|
    headers[header.delete_prefix('HTTP_')] = value if header.start_with? 'HTTP_'
  end

  headers['CONTENT_TYPE'] = headers['Content-Type'] = env['CONTENT_TYPE']
end

# Echo Methods
get '/api/echo' do
  prepare_headers(headers)

  [200, headers, '']
end

post '/api/echo' do
  prepare_headers(headers)

  [200, headers, request.body.read]
end

patch '/api/echo' do
  prepare_headers(headers)

  [200, headers, request.body.read]
end

put '/api/echo' do
  prepare_headers(headers)

  [200, headers, request.body.read]
end

delete '/api/echo' do
  prepare_headers(headers)

  [200, headers, '']
end
