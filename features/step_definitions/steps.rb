require 'uri'
require 'httparty'
require 'pathname'
require 'rspec/expectations'
require 'json'

Given('the following settings') do |settings_json|
  @requests = []
  settings = JSON.parse(settings_json)
  @port = settings['port']

  File.open(settings_path, 'w') do |f|
    f.write(settings_json)
  end
end

Given('the following route in {string}') do |route, route_json|
  route_path = routes_path + Pathname.new(route)
  route = JSON.parse(route_json)
  route = route.map do |k, v|
    v['host'] = ENV[v['host']]
    [k, v]
  end.to_h
  route_json = route.to_json

  File.open(route_path, 'w') do |f|
    f.write(route_json)
  end
end

Given('the following caches') do |cache_json|
  File.open(caches_path, 'w') do |f|
    f.write(cache_json)
  end

  command = <<-CARP
    docker run -d -p #{@port}:#{@port} --name #{ENV['CARP_HOST']} -v #{ENV['VOLUME']}:/carp/config --network #{ENV['NETWORK']} --network-alias #{ENV['CARP_HOST']} --rm carp-suite/carp:#{ENV['VERSION']} /carp/config > /dev/null
  CARP

  system(command)
  sleep(ENV['CARP_DELAY'].to_i)
end

When('a {string} request is made to path {string} with body') do |method, route, body|
  request = {
    body: body,
    route: route,
    method: method
  }
  @requests << request
end

When('query parameters') do |query_params_table|
  query_params = query_params_table.to_hash[2..]

  unless query_params.length.zero?
    query_string = query_params.map { |name, value| "#{name}=#{value}" }.to_a.join('&')
    @requests.last[:route] = "#{@requests.last[:route]}?#{query_string}" unless query_string.empty?
  end
end

When('headers') do |headers_table|
  @responses = []
  @requests.last[:headers] = Hash[headers_table.to_hash[2..]]
  @requests.last[:url] = "http://#{ENV['CARP_HOST']}:#{@port}#{@requests.last[:route]}"
end

When('all requests are issued') do
  @requests.each do |request|
    case request[:method]
    when 'GET'
      @responses << HTTParty.get(request[:url], headers: request[:headers])

    when 'DELETE'
      @responses << HTTParty.delete(request[:url], headers: request[:headers])

    when 'POST'
      args = {}
      args[:body] = request[:body] unless request[:body].empty?
      args[:headers] = request[:headers] unless request[:headers].empty?

      @responses << HTTParty.post(request[:url], **args)

    when 'PUT'
      args = {}
      args[:body] = request[:body] unless request[:body].empty?
      args[:headers] = request[:headers] unless request[:headers].empty?

      @responses << HTTParty.put(request[:url], **args)

    when 'PATCH'
      args = {}
      args[:body] = request[:body] unless request[:body].empty?
      args[:headers] = request[:headers] unless request[:headers].empty?

      @responses << HTTParty.patch(request[:url], **args)
    end
  end
end

Then('the {int} response should be successful') do |index|
  @index = index - 1
  expect(@responses[@index].success?).to be true
end

Then('have the body') do |body|
  expect(@responses[@index].body).to eq(body)
end

Then('the headers') do |headers_table|
  headers_table.to_hash[2..].each do |header, value|
    expect(@responses[@index].headers[header.swapcase]).to eq(value)
  end
end

After do |_scenario|
  system("docker stop #{ENV['CARP_HOST']} > /dev/null")
end
