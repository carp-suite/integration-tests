Feature: Proxies PATCH requests
  Scenario: PATCH requests with no body and with query parameters and with no Content-Type
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "PATCH": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "PATCH" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name         | value      |
      |--------------|------------|
      | a            | b          |
    And headers
      | name         | value      |
      |--------------|------------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      | name              | value      |
      |-------------------|------------|
      | CARP_URL          | /api/echo  |
      | CARP_QUERY_STRING | a=b        |
      | Content-Length    | 0          |

  Scenario: PATCH requests with no body and with query parameteres and with headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "PATCH": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "PATCH" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      | a    |  b    |
      | c    |  d    |
    And headers
      | name            | value      |
      |-----------------|------------|
      | header-1        | value-1    |
      | header-a-2      | avalue-2   |
      | Content-Type    | text/plain |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      | name              | value      |
      |-------------------|------------|
      | CARP_URL          | /api/echo  |
      | CARP_QUERY_STRING | a=b&c=d    |
      | HEADER_1          | value-1    |
      | HEADER_A_2        | avalue-2   |
      | CONTENT_TYPE      | text/plain |
      | Content-Length    | 0          |

  Scenario: PATCH requests with body and with query parameters and with headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "PATCH": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "PATCH" request is made to path "/api/echo" with body 
      """
      {
        "hello": "world"
      }
      """   
    And query parameters
      | name | value |
      |------|-------|
      | a    | b     |
      | c    | d     |
    And headers
      | name          | value            |
      |---------------|------------------|
      | header-1      | value-1          |
      | header-a-2    | avalue-2         |
      | Cookie        | cookie-1=value-1 |
      | Content-Type  | application/json |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      {
        "hello": "world"
      }
      """
    And the headers
      | name              | value            |
      |-------------------|------------------|
      | CARP_URL          | /api/echo        |
      | CARP_QUERY_STRING | a=b&c=d          |
      | HEADER_1          | value-1          |
      | HEADER_A_2        | avalue-2         |
      | COOKIE            | cookie-1=value-1 |
      | CONTENT_TYPE      | application/json |
      | Content-Length    | 22               |
      | Content-Type      | application/json |
