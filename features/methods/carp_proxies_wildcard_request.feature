Feature: Proxies ANY requests
  Scenario: PUT requests with WILDCARD and no body and with query parameters and with no Content-Type
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "PUT" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name         | value      |
      |--------------|------------|
      | a            | b          |
    And headers
      | name         | value      |
      |--------------|------------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      | name              | value      |
      |-------------------|------------|
      | CARP_URL          | /api/echo  |
      | CARP_QUERY_STRING | a=b        |

  Scenario: PUT requests with WILDCARD and with no body and with query parameteres and with headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "PUT" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      | a    |  b    |
      | c    |  d    |
    And headers
      | name         | value      |
      |--------------|------------|
      | header-1     | value-1    |
      | header-a-2   | avalue-2   |
      | Content-Type | text/plain |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      | name              | value      |
      |-------------------|------------|
      | CARP_URL          | /api/echo  |
      | CARP_QUERY_STRING | a=b&c=d    |
      | HEADER_1          | value-1    |
      | HEADER_A_2        | avalue-2   |
      | CONTENT_TYPE      | text/plain |

  Scenario: PUT requests with WILDCARD with body and with query parameters and with headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "PUT" request is made to path "/api/echo" with body 
      """
      {
        "hello": "world"
      }
      """   
    And query parameters
      | name | value |
      |------|-------|
      | a    | b     |
      | c    | d     |
    And headers
      | name          | value            |
      |---------------|------------------|
      | header-1      | value-1          |
      | header-a-2    | avalue-2         |
      | Cookie        | cookie-1=value-1 |
      | Content-Type  | application/json |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      {
        "hello": "world"
      }
      """
    And the headers
      | name              | value            |
      |-------------------|------------------|
      | CARP_URL          | /api/echo        |
      | CARP_QUERY_STRING | a=b&c=d          |
      | HEADER_1          | value-1          |
      | HEADER_A_2        | avalue-2         |
      | COOKIE            | cookie-1=value-1 |
      | CONTENT_TYPE      | application/json |
      | Content-Length    | 22               |
      | Content-Type      | application/json |

  Scenario: POST requests with WILDCARD with no body and with query parameters and with no Content-Type
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "POST" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name         | value      |
      |--------------|------------|
      | a            | b          |
    And headers
      | name         | value      |
      |--------------|------------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      | name              | value      |
      |-------------------|------------|
      | CARP_URL          | /api/echo  |
      | CARP_QUERY_STRING | a=b        |

  Scenario: POST requests with WILDCARD with no body and with query parameteres and with headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "POST" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      | a    |  b    |
      | c    |  d    |
    And headers
      | name         | value      |
      |--------------|------------|
      | header-1     | value-1    |
      | header-a-2   | avalue-2   |
      | Content-Type | text/plain |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      | name              | value      |
      |-------------------|------------|
      | CARP_URL          | /api/echo  |
      | CARP_QUERY_STRING | a=b&c=d    |
      | HEADER_1          | value-1    |
      | HEADER_A_2        | avalue-2   |
      | CONTENT_TYPE      | text/plain |

  Scenario: POST requests with WILDCARD with body and with query parameters and with headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "POST" request is made to path "/api/echo" with body 
      """
      {
        "hello": "world"
      }
      """   
    And query parameters
      | name | value |
      |------|-------|
      | a    | b     |
      | c    | d     |
    And headers
      | name          | value            |
      |---------------|------------------|
      | header-1      | value-1          |
      | header-a-2    | avalue-2         |
      | Cookie        | cookie-1=value-1 |
      | Content-Type  | application/json |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      {
        "hello": "world"
      }
      """
    And the headers
      | name              | value            |
      |-------------------|------------------|
      | CARP_URL          | /api/echo        |
      | CARP_QUERY_STRING | a=b&c=d          |
      | HEADER_1          | value-1          |
      | HEADER_A_2        | avalue-2         |
      | COOKIE            | cookie-1=value-1 |
      | CONTENT_TYPE      | application/json |
      | Content-Length    | 22               |
      | Content-Type      | application/json |

  Scenario: PATCH requests with WILDCARD with no body and with query parameters and with no Content-Type
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "PATCH" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name         | value      |
      |--------------|------------|
      | a            | b          |
    And headers
      | name         | value      |
      |--------------|------------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      | name              | value      |
      |-------------------|------------|
      | CARP_URL          | /api/echo  |
      | CARP_QUERY_STRING | a=b        |

  Scenario: PATCH requests with WILDCARD with no body and with query parameteres and with headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "PATCH" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      | a    |  b    |
      | c    |  d    |
    And headers
      | name         | value      |
      |--------------|------------|
      | header-1     | value-1    |
      | header-a-2   | avalue-2   |
      | Content-Type | text/plain |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      | name              | value      |
      |-------------------|------------|
      | CARP_URL          | /api/echo  |
      | CARP_QUERY_STRING | a=b&c=d    |
      | HEADER_1          | value-1    |
      | HEADER_A_2        | avalue-2   |
      | CONTENT_TYPE      | text/plain |

  Scenario: PATCH requests with WILDCARD with body and with query parameters and with headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "PATCH" request is made to path "/api/echo" with body 
      """
      {
        "hello": "world"
      }
      """   
    And query parameters
      | name | value |
      |------|-------|
      | a    | b     |
      | c    | d     |
    And headers
      | name          | value            |
      |---------------|------------------|
      | header-1      | value-1          |
      | header-a-2    | avalue-2         |
      | Cookie        | cookie-1=value-1 |
      | Content-Type  | application/json |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      {
        "hello": "world"
      }
      """
    And the headers
      | name              | value            |
      |-------------------|------------------|
      | CARP_URL          | /api/echo        |
      | CARP_QUERY_STRING | a=b&c=d          |
      | HEADER_1          | value-1          |
      | HEADER_A_2        | avalue-2         |
      | COOKIE            | cookie-1=value-1 |
      | CONTENT_TYPE      | application/json |
      | Content-Length    | 22               |
      | Content-Type      | application/json |

  Scenario: GET requests with WILDCARD is empty
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "GET" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
    And headers
      | name | value |
      |------|-------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |   value   |
      |-------------------|-----------|
      | CARP_URL          | /api/echo |

  Scenario: GET requests with WILDCARD and query parameters
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "GET" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      |   a  |   b   |
    And headers
      |   name   |   value   |
      |----------|-----------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |   value   |
      |-------------------|-----------|
      | CARP_URL          | /api/echo |
      | CARP_QUERY_STRING |  a=b      |

  Scenario: GET requests with WILDCARD, query parameters and headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "GET" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      |  a   |   b   |
      |  c   |   d   |
    And headers
      |   name       |  value    |
      |--------------|-----------|
      |   header-1   |  value-1  |
      |  header-a-2  |  avalue-2 |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |   value   |
      |-------------------|-----------|
      | CARP_URL          | /api/echo |
      | CARP_QUERY_STRING | a=b&c=d   |
      | HEADER_1          | value-1   |
      | HEADER_A_2        | avalue-2  |

  Scenario: GET requests with with WILDCARD, query parameters, headers and cookies
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "GET" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      |  a   |   b   |
      |  c   |   d   |
    And headers
      |   name       |  value            |
      |--------------|-------------------|
      |   header-1   |  value-1          |
      |  header-a-2  |  avalue-2         |
      |  Cookie      |  cookie-1=value-1 |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |      value       |
      |-------------------|------------------|
      | CARP_URL          | /api/echo        |
      | CARP_QUERY_STRING | a=b&c=d          |
      | HEADER_1          | value-1          |
      | HEADER_A_2        | avalue-2         |
      | COOKIE            | cookie-1=value-1 |

  Scenario: DELETE requests with WILDCARD is empty
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "DELETE" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
    And headers
      | name | value |
      |------|-------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |   value   |
      |-------------------|-----------|
      | CARP_URL          | /api/echo |

  Scenario: DELETE requests with WILDCARD and query parameters
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "DELETE" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      |   a  |   b   |
    And headers
      |   name   |   value   |
      |----------|-----------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |   value   |
      |-------------------|-----------|
      | CARP_URL          | /api/echo |
      | CARP_QUERY_STRING |  a=b      |

  Scenario: DELETE requests with WILDCARD, query parameters and headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "DELETE" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      |  a   |   b   |
      |  c   |   d   |
    And headers
      |   name       |  value    |
      |--------------|-----------|
      |   header-1   |  value-1  |
      |  header-a-2  |  avalue-2 |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |   value   |
      |-------------------|-----------|
      | CARP_URL          | /api/echo |
      | CARP_QUERY_STRING | a=b&c=d   |
      | HEADER_1          | value-1   |
      | HEADER_A_2        | avalue-2  |

  Scenario: DELETE requests with WILDCARD and query parameters, headers and cookies
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "*": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "DELETE" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      |  a   |   b   |
      |  c   |   d   |
    And headers
      |   name       |  value            |
      |--------------|-------------------|
      |   header-1   |  value-1          |
      |  header-a-2  |  avalue-2         |
      |  Cookie      |  cookie-1=value-1 |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |      value       |
      |-------------------|------------------|
      | CARP_URL          | /api/echo        |
      | CARP_QUERY_STRING | a=b&c=d          |
      | HEADER_1          | value-1          |
      | HEADER_A_2        | avalue-2         |
      | COOKIE            | cookie-1=value-1 |
