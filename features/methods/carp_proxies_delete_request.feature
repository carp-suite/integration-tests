Feature: Proxies WILDCARD requests
  Scenario: DELETE requests is empty
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "DELETE": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "DELETE" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
    And headers
      | name | value |
      |------|-------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |   value   |
      |-------------------|-----------|
      | CARP_URL          | /api/echo |

  Scenario: DELETE requests with query parameters
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "DELETE": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "DELETE" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      |   a  |   b   |
    And headers
      |   name   |   value   |
      |----------|-----------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |   value   |
      |-------------------|-----------|
      | CARP_URL          | /api/echo |
      | CARP_QUERY_STRING |  a=b      |

  Scenario: DELETE requests with query parameters and headers
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "DELETE": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "DELETE" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      |  a   |   b   |
      |  c   |   d   |
    And headers
      |   name       |  value    |
      |--------------|-----------|
      |   header-1   |  value-1  |
      |  header-a-2  |  avalue-2 |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |   value   |
      |-------------------|-----------|
      | CARP_URL          | /api/echo |
      | CARP_QUERY_STRING | a=b&c=d   |
      | HEADER_1          | value-1   |
      | HEADER_A_2        | avalue-2  |

  Scenario: DELETE requests with query parameters, headers and cookies
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "echo.json"
      """
      {
        "DELETE": {
          "host": "ECHO_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "DELETE" request is made to path "/api/echo" with body 
      """
      """   
    And query parameters
      | name | value |
      |------|-------|
      |  a   |   b   |
      |  c   |   d   |
    And headers
      |   name       |  value            |
      |--------------|-------------------|
      |   header-1   |  value-1          |
      |  header-a-2  |  avalue-2         |
      |  Cookie      |  cookie-1=value-1 |
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      """
    And the headers
      |       name        |      value       |
      |-------------------|------------------|
      | CARP_URL          | /api/echo        |
      | CARP_QUERY_STRING | a=b&c=d          |
      | HEADER_1          | value-1          |
      | HEADER_A_2        | avalue-2         |
      | COOKIE            | cookie-1=value-1 |
