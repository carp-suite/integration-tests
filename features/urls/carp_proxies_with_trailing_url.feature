Feature: Proxies with trailing URL
  Scenario: Trailing URL with base url and route set
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "prefix.json"
      """
      {
        "*": {
          "host": "TRAILING_SERVER",
          "invalidates": [],
          "cache": null
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "unneeded",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "GET" request is made to path "/api/prefix/trailing/url" with body
      """
      """
    And query parameters
      | name | value |
      |------|-------|
    And headers
      | name | value |
      |------|-------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      {
        "path": "/trailing/url"
      }
      """
    And the headers
      | name       | value               |
      |------------|---------------------|
      | Set-Cookie | cookie1=name;Path=/ |

