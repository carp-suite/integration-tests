require 'pathname'
module Helpers
  def caches_path
    carp_path + Pathname.new('caches.json')
  end

  def routes_path
    carp_path + Pathname.new('routes')
  end

  def settings_path
    carp_path + Pathname.new('settings.json')
  end

  def carp_path
    Pathname.new('config')
  end
end

World(Helpers)
