Feature: Caches response according to route cache usage
  Scenario: GET request is cached
    Given the following settings
      """
      {
        "address": "0.0.0.0",
        "port": 3000,
        "baseUrl": "/api",
        "plugins": []
      }
      """
    And the following route in "cache.json"
      """
      {
        "GET": {
          "host": "CACHE_SERVER",
          "invalidates": [],
          "cache": {
            "name": "First Cache",
            "queryParameters": [],
            "headers": []
          }
        }
      }
      """
    And the following caches
      """
      [
        {
          "name": "First Cache",
          "ttl": 3600,
          "dependencies": []
        }
      ]
      """
    When a "GET" request is made to path "/api/cache" with body 
      """
      """   
    And query parameters
      | name       | value         |
      |------------|---------------|
      | request_id | first_request |
    And headers
      | name | value |
      |------|-------|
    When a "GET" request is made to path "/api/cache" with body
      """
      """
    And query parameters
      | name       | value          |
      |------------|----------------|
      | request_id | second_request |
    And headers
      | name | value |
      |------|-------|
    And all requests are issued
    Then the 1 response should be successful
    And have the body
      """
      <html>
        <head>
          <title>Div Align Attribute</title>
        </head>
        <body>
          <div align="left">
            Lorem ipsum dolor sit amet, officia excepteur ex fugiat 
            reprehenderit enim labore culpa sint ad nisi Lorem 
            pariatur mollit ex esse exercitation amet. Nisi anim 
            cupidatat excepteur officia. Reprehenderit nostrud 
            nostrud ipsum Lorem est aliquip amet voluptate voluptate 
            dolor minim nulla est proident. Nostrud officia pariatur 
            ut officia. Sit irure elit esse ea nulla sunt ex occaecat 
            reprehenderit commodo officia dolor Lorem duis laboris 
            cupidatat officia voluptate. Culpa proident adipisicing 
            id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. 
            Aliqua reprehenderit commodo ex non excepteur duis sunt 
            velit enim. Voluptate laboris sint cupidatat ullamco ut 
            ea consectetur et est culpa et culpa duis.
          </div>
          <div align="right">
            Lorem ipsum dolor sit amet, officia excepteur ex fugiat 
            reprehenderit enim labore culpa sint ad nisi Lorem 
            pariatur mollit ex esse exercitation amet. Nisi anim 
            cupidatat excepteur officia. Reprehenderit nostrud 
            nostrud ipsum Lorem est aliquip amet voluptate voluptate 
            dolor minim nulla est proident. Nostrud officia pariatur 
            ut officia. Sit irure elit esse ea nulla sunt ex occaecat 
            reprehenderit commodo officia dolor Lorem duis laboris 
            cupidatat officia voluptate. Culpa proident adipisicing 
            id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. 
            Aliqua reprehenderit commodo ex non excepteur duis sunt 
            velit enim. Voluptate laboris sint cupidatat ullamco ut 
            ea consectetur et est culpa et culpa duis.
          </div>
          <div align="center">
            Lorem ipsum dolor sit amet, officia excepteur ex fugiat 
            reprehenderit enim labore culpa sint ad nisi Lorem 
            pariatur mollit ex esse exercitation amet. Nisi anim 
            cupidatat excepteur officia. Reprehenderit nostrud 
            nostrud ipsum Lorem est aliquip amet voluptate voluptate 
            dolor minim nulla est proident. Nostrud officia pariatur 
            ut officia. Sit irure elit esse ea nulla sunt ex occaecat 
            reprehenderit commodo officia dolor Lorem duis laboris 
            cupidatat officia voluptate. Culpa proident adipisicing 
            id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. 
            Aliqua reprehenderit commodo ex non excepteur duis sunt 
            velit enim. Voluptate laboris sint cupidatat ullamco ut 
            ea consectetur et est culpa et culpa duis.
          </div>
          <div align="justify">
            Lorem ipsum dolor sit amet, officia excepteur ex fugiat 
            reprehenderit enim labore culpa sint ad nisi Lorem 
            pariatur mollit ex esse exercitation amet. Nisi anim 
            cupidatat excepteur officia. Reprehenderit nostrud 
            nostrud ipsum Lorem est aliquip amet voluptate voluptate 
            dolor minim nulla est proident. Nostrud officia pariatur 
            ut officia. Sit irure elit esse ea nulla sunt ex occaecat 
            reprehenderit commodo officia dolor Lorem duis laboris 
            cupidatat officia voluptate. Culpa proident adipisicing 
            id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. 
            Aliqua reprehenderit commodo ex non excepteur duis sunt 
            velit enim. Voluptate laboris sint cupidatat ullamco ut 
            ea consectetur et est culpa et culpa duis.
          </div>
        </body>
      </html>

      """
    And the headers
      |     name     |   value       |
      |--------------|---------------|
      | X-REQUEST-ID | first_request |
    Then the 2 response should be successful
    And have the body
      """
      <html>
        <head>
          <title>Div Align Attribute</title>
        </head>
        <body>
          <div align="left">
            Lorem ipsum dolor sit amet, officia excepteur ex fugiat 
            reprehenderit enim labore culpa sint ad nisi Lorem 
            pariatur mollit ex esse exercitation amet. Nisi anim 
            cupidatat excepteur officia. Reprehenderit nostrud 
            nostrud ipsum Lorem est aliquip amet voluptate voluptate 
            dolor minim nulla est proident. Nostrud officia pariatur 
            ut officia. Sit irure elit esse ea nulla sunt ex occaecat 
            reprehenderit commodo officia dolor Lorem duis laboris 
            cupidatat officia voluptate. Culpa proident adipisicing 
            id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. 
            Aliqua reprehenderit commodo ex non excepteur duis sunt 
            velit enim. Voluptate laboris sint cupidatat ullamco ut 
            ea consectetur et est culpa et culpa duis.
          </div>
          <div align="right">
            Lorem ipsum dolor sit amet, officia excepteur ex fugiat 
            reprehenderit enim labore culpa sint ad nisi Lorem 
            pariatur mollit ex esse exercitation amet. Nisi anim 
            cupidatat excepteur officia. Reprehenderit nostrud 
            nostrud ipsum Lorem est aliquip amet voluptate voluptate 
            dolor minim nulla est proident. Nostrud officia pariatur 
            ut officia. Sit irure elit esse ea nulla sunt ex occaecat 
            reprehenderit commodo officia dolor Lorem duis laboris 
            cupidatat officia voluptate. Culpa proident adipisicing 
            id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. 
            Aliqua reprehenderit commodo ex non excepteur duis sunt 
            velit enim. Voluptate laboris sint cupidatat ullamco ut 
            ea consectetur et est culpa et culpa duis.
          </div>
          <div align="center">
            Lorem ipsum dolor sit amet, officia excepteur ex fugiat 
            reprehenderit enim labore culpa sint ad nisi Lorem 
            pariatur mollit ex esse exercitation amet. Nisi anim 
            cupidatat excepteur officia. Reprehenderit nostrud 
            nostrud ipsum Lorem est aliquip amet voluptate voluptate 
            dolor minim nulla est proident. Nostrud officia pariatur 
            ut officia. Sit irure elit esse ea nulla sunt ex occaecat 
            reprehenderit commodo officia dolor Lorem duis laboris 
            cupidatat officia voluptate. Culpa proident adipisicing 
            id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. 
            Aliqua reprehenderit commodo ex non excepteur duis sunt 
            velit enim. Voluptate laboris sint cupidatat ullamco ut 
            ea consectetur et est culpa et culpa duis.
          </div>
          <div align="justify">
            Lorem ipsum dolor sit amet, officia excepteur ex fugiat 
            reprehenderit enim labore culpa sint ad nisi Lorem 
            pariatur mollit ex esse exercitation amet. Nisi anim 
            cupidatat excepteur officia. Reprehenderit nostrud 
            nostrud ipsum Lorem est aliquip amet voluptate voluptate 
            dolor minim nulla est proident. Nostrud officia pariatur 
            ut officia. Sit irure elit esse ea nulla sunt ex occaecat 
            reprehenderit commodo officia dolor Lorem duis laboris 
            cupidatat officia voluptate. Culpa proident adipisicing 
            id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. 
            Aliqua reprehenderit commodo ex non excepteur duis sunt 
            velit enim. Voluptate laboris sint cupidatat ullamco ut 
            ea consectetur et est culpa et culpa duis.
          </div>
        </body>
      </html>

      """
    And the headers
      |     name     |   value       |
      |--------------|---------------|
      | X-REQUEST-ID | first_request |
