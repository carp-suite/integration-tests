FROM ruby:3.2.0-alpine

RUN apk update && apk add --no-cache build-base docker openrc
WORKDIR /integration-tests/

RUN rc-update add docker boot

COPY Gemfile Gemfile
RUN bundle install

COPY Rakefile Rakefile
COPY servers servers
COPY features features
COPY Dockerfile.* ./

ARG DELAY_PORT=5000
ARG ECHO_PORT=5000
ARG TRAILING_PORT=5000
ARG CACHE_PORT=5000

ARG DELAY_HOST=delay-server
ARG ECHO_HOST=echo-server
ARG TRAILING_HOST=trailing-server
ARG CACHE_HOST=trailing-server
ARG SETUP=1

ENV DELAY_HOST=$DELAY_HOST
ENV ECHO_HOST=$ECHO_HOST
ENV TRAILING_HOST=$TRAILING_HOST
ENV CACHE_HOST=$CACHE_HOST
ENV SETUP=$SETUP

ENV CACHE_SERVER="http://$CACHE_HOST:$CACHE_PORT"
ENV DELAY_SERVER="http://$DELAY_HOST:$DELAY_PORT"
ENV ECHO_SERVER="http://$ECHO_HOST:$ECHO_PORT"
ENV TRAILING_SERVER="http://$TRAILING_HOST:$TRAILING_PORT"

CMD ["rake", "features"]
