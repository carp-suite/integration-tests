# frozen_string_literal: true

require 'cucumber'
require 'pathname'
require 'rubygems'
require 'cucumber/rake/task'

directory 'config/routes'

task 'cache' do |_task|
  sh 'docker', 'build', '-t', "carp-suite/cache-server:#{ENV['VERSION']}", '-f', 'Dockerfile.cache', '.'
  sh 'docker', 'run', '--network', ENV['NETWORK'], '--network-alias', ENV['CACHE_HOST'],
     '--expose', '5000', '--rm', '--name', ENV['CACHE_HOST'], '-d',
     '-p', '5000:5000', "carp-suite/cache-server:#{ENV['VERSION']}"
end

task 'trailing' do |_task|
  sh 'docker', 'build', '-t', "carp-suite/trailing-server:#{ENV['VERSION']}", '-f', 'Dockerfile.trailing', '.'
  sh 'docker', 'run', '--network', ENV['NETWORK'], '--network-alias', ENV['TRAILING_HOST'],
     '--expose', '5000', '--rm', '--name', ENV['TRAILING_HOST'], '-d',
     '-p', '5500:5000', "carp-suite/trailing-server:#{ENV['VERSION']}"
end

task 'delay', [:delay] do |_task, args|
  sh 'docker', 'build', '-t', "carp-suite/delay-server:#{ENV['VERSION']}", '-f', 'Dockerfile.delay', '.'
  sh 'docker', 'run', '--network', ENV['NETWORK'], '--network-alias', ENV['DELAY_HOST'], '--rm',
     '-e', "DELAY=#{args[:delay]}", '--name', ENV['DELAY_HOST'], '-d', '-p', '4500:5000', '--expose', '5000',
     "carp-suite/delay-server:#{ENV['VERSION']}"
end

task 'echo' do |_task|
  sh 'docker', 'build', '-t', "carp-suite/echo-server:#{ENV['VERSION']}", '-f', 'Dockerfile.echo', '.'
  sh 'docker', 'run', '--network', ENV['NETWORK'], '--network-alias', ENV['ECHO_HOST'], '--rm', '--name',
     ENV['ECHO_HOST'], '-d', '-p', '6500:5000', '--expose', '5000', "carp-suite/echo-server:#{ENV['VERSION']}"
end

Cucumber::Rake::Task.new(:features) do |t|
  t.cucumber_opts = ['--format pretty', '--publish-quiet']
end

task features: %w[config/routes cache echo delay trailing]
